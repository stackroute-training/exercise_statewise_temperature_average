﻿using System;

namespace exercise_state_wise_temperature_average
{
    public class Program
    {
        static void Main(string[] args)
        {
            /*declare array for collecting temperature of cities of 3 states
             *
             *first state provides temperature of 3 cities
             *second state provides temperature of 7 cities
             *third state provides temperature of 4 cities
            */
            int[] state1 = { 20, 30, 25 };
            int[] state2 = { 20, 30, 25, 40, 45, 10, 15 };
            int[] state3 = { 25, 5, -10 };
            int[][] arr = { state1, state2, state3 };
            /*
             * Call GetStateWiseAverage() with state position and temperature data
             */
            int average = GetStateWiseAverage(0,arr);
            /*
             * Display Average
             */
            Console.WriteLine(average);
        }

        /* 
         * Method calculates average of temperature for the state whose position is provided
         */

        public static int GetStateWiseAverage(int index, int[][] temp)
        {
            // put here logic to calculate average
            int[] state = temp[index];
            int count = 0;
            int avg = 0;
            foreach (int n in state)
            {
                count += n;
            }
            avg = count / state.Length;
            return avg;
        }
    }
}
